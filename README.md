# README #

### Security test Observatory with postman newman ###

Realização de teste de segurança fazendo uso da api do Mozilla Observatory com chamadas encadeadas do postman + newman.

GET https://http-observatory.security.mozilla.org/api/v1/analyze?host={{url}}&rescan=true&verbose=true&hidden=true

- Realiza  a request das url's conforme aquivo de configuração (scenarios.json).
- Recupera o idScan gravando em variavel de ambiente.


GET https://http-observatory.security.mozilla.org/api/v1/getScanResults?scan={{idScan}}

- Realiza request passando como paramentro o idScan recuperado da request anterior
- Realiza testes validando o retorno da api 



Para rodar os testes do projeto é necessário o pacote newman e o pacote newman-reporter-htmlextra para a geração de relatórios.
```
npm install -g newman
npm install -g newman-reporter-htmlextra
             
```

### Exportação de scripts do postman e execução com newman ###

- Podemos exportar os scripts a collection do postman (.postman_collection.json) assim como as variáveis de ambiente(.postman_environment).
- Para execução do teste é necessário executar a linha conforme descrição abaixo:


```
newman run collection_name.postman_collection.json -e environment_name.postman_environment.json -d cenarios.json --delay-request 2000 -r cli,htmlextra
```

